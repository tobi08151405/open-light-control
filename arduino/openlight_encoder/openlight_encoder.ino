byte direction[] = {2};
const int encCount = sizeof(direction)/sizeof(direction[0]);
byte pulse[] = {3};
byte laststate[] = {LOW};

int curDeriva[encCount];

void setup() {
  Serial.begin(115200);
  Serial.setTimeout(2);
  for (int encIndex=0; encIndex<encCount; encIndex++) {
    pinMode(direction[encIndex], INPUT);
    pinMode(pulse[encIndex], INPUT);
  }
  while (!Serial) {
    ;
  }
  // Serial.println(encCount);
}

void loop() {
  for (int encIndex=0; encIndex<encCount; encIndex++) {
    int curValue = digitalRead(pulse[encIndex]);
    if ((laststate[encIndex] == LOW) && (curValue == HIGH))
    {
      if (digitalRead(direction[encIndex]) == LOW)
      {
        curDeriva[encIndex]++;
      }
      else {
        curDeriva[encIndex]--;
      }
      Serial.print(encIndex);
      Serial.print(":");
      Serial.println(curDeriva[encIndex]);
    }
    laststate[encIndex] = curValue;
  }
}
