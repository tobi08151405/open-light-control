import json
from typing import Any, Union

#import pdb

from GlobalVar import typ_to_func, typ_to_addr, typ_to_func_new
from Lamp import *
from PyQt5.QtGui import QColor

def mode_to_func(json_dic: 'dict[str, Any]', mode: 'dict[str, str]'):
    color = ''
    gobo = ''
    color_slots: 'Union[str, list[dict[str, str]]]'
    color_slots = ''
    gobo_slots: 'Union[str, list[str]]'
    gobo_slots = ''
    name = json_dic['name']
    typ_mode_name = "{0:s} {1:s}".format(name, mode['name'])
    if 'wheels' in json_dic.keys():
        for wheel in list(json_dic['wheels'].keys()):
            if 'Color' in wheel:
                color += 'Wheel'
                color_slots = []
                for slot in json_dic['wheels'][wheel]['slots']:
                    if slot['type'] == 'Open':
                        slot_name = 'Open'
                        slot_color = '#ffffff'
                    elif slot['type'] == 'Color':
                        if 'colorTemperature' in slot.keys():
                            slot_name = slot['colorTemperature']
                            slot_color = '#ffffff'
                        elif 'colors' in slot.keys():
                            if 'name' in slot.keys():
                                slot_name = slot['name']
                            else:
                                slot_name = 'Color'
                            slot_color = slot['colors'][0]
                    try:
                        if (slot_name and slot_color):
                            color_slots.append(
                                {'name': slot_name, 'color': slot_color})
                    except:
                        pass

            if 'Gobo' in wheel:
                gobo = 'Wheel'
                gobo_slots = []
                for slot in json_dic['wheels'][wheel]['slots']:
                    if slot['type'] == 'Open':
                        slot_name = 'Open'
                    elif slot['type'] == 'Gobo':
                        slot_name = slot['name']
                    try:
                        if (slot_name):
                            gobo_slots.append(slot_name)
                    except:
                        pass

    if all(any(channel in i for i in mode['channels']) for channel in ['Red', 'Green', 'Blue']) or all(any(channel in i for i in mode['channels']) for channel in ['Cyan', 'Magenta', 'Yellow']):
        color += 'RGB'
        if 'Amber' in mode['channels']:
            color += 'A'
        if 'White' in mode['channels']:
            color += 'W'
    if any(channel in mode['channels'] for channel in ['Intensity', 'Dimmer']):
        dimmer = True
    else:
        dimmer = False
    if any('Pan' in i for i in mode['channels']):
        pan = True
    else:
        pan = False
    if any('Tilt' in i for i in mode['channels']):
        tilt = True
    else:
        tilt = False

    if not color:
        color = False
    if not gobo:
        gobo = False

    typ_to_func[typ_mode_name] = {"ChannelSpan": len(
        mode['channels']), "Dimmer": dimmer, "Color": color, "Gobo": gobo, "Pan": pan, "Tilt": tilt}
    if color_slots:
        typ_to_func[typ_mode_name]["ColorWheel"] = color_slots
    if gobo_slots:
        typ_to_func[typ_mode_name]["GoboWheel"] = gobo_slots

def typ_to_modes_func(json_dic: 'dict[str, Any]'):
    for mode in json_dic['modes']:
        mode_to_func(json_dic, mode)


def create_typ_to_func(json_names: 'list[str]'):
    for json_name in json_names:
        try:
            with open(json_name) as json_file:
                json_dic = json.load(json_file)
                typ_to_modes_func(json_dic)
        except FileNotFoundError:
            print("file {0:s} not found!".format(json_name))


def mode_to_addr(json_dic: 'dict[str, Any]', mode: 'dict[str, str]'):
    name = json_dic['name']
    typ_mode_name: str
    typ_mode_name = "{0:s} {1:s}".format(name, mode['name'])
    typ_to_addr[typ_mode_name] = {}
    for channel_name in mode['channels']:
        try:
            channel = json_dic['availableChannels'][channel_name]
            channel_num = mode['channels'].index(channel_name)
            channel_mode = ''
            split_list: 'list[Any]'
            split_list = []
            if 'capability' in channel:
                channel_mode = "normal"
            elif 'capabilities' in channel:
                channel_mode = "split"
                split_list = []
            if channel_name == "Intensity":
                channel_name = "Dimmer"
            typ_to_addr[typ_mode_name][channel_name] = {
                "Channel": channel_num, "Mode": channel_mode}
            if channel_mode == "split" and split_list:
                typ_to_addr[typ_mode_name][channel_name]['Split'] = {
                    split_list}
        except KeyError:
            pass


def typ_to_modes_addr(json_dic: 'dict[str, Any]'):
    for mode in json_dic['modes']:
        mode_to_addr(json_dic, mode)


def create_typ_to_addr(json_names: 'list[str]'):
    for json_name in json_names:
        try:
            with open(json_name) as json_file:
                json_dic = json.load(json_file)
                typ_to_modes_addr(json_dic)
        except FileNotFoundError:
            print("file {0:s} not found!".format(json_name))


def mode_to_func_new(json_dic: 'dict[str, Any]', mode: 'dict[str, str]') -> LampCapabilities:
    cap = LampCapabilities()
    colwheel: 'list[ColorWheelSlot]'
    colwheel = []
    gobowheel: 'list[GoboWheelSlot]'
    gobowheel = []
    colcap = LampColorCapabilities()

    name = json_dic['name']
    typ_mode_name = "{0:s} {1:s}".format(name, mode['name'])

    if "wheels" in json_dic:
        for wheel_name, wheel_values in json_dic.get("wheels", {}).items():
            if "Color" in wheel_name:
                for slot in wheel_values.get("slots", []):
                    wheel_slot = ColorWheelSlot(
                        "N/A", QColor.fromRgba64(0, 0, 0, 0))
                    if slot.get('type', "") == 'Open':
                        wheel_slot.name = 'Open'
                        wheel_slot.color = QColor('#ffffff')
                    elif slot.get('type', "") == 'Color':
                        if 'colorTemperature' in slot:
                            wheel_slot.name = slot.get(
                                'colorTemperature', "N/A")
                            wheel_slot.color = QColor('#ffffff')
                        elif 'colors' in slot:
                            wheel_slot.name = slot.get('name', 'Color')
                            wheel_slot.color = QColor(
                                slot.get('colors', ["#ffffff"])[0])
                    colwheel.append(wheel_slot)
            elif "Gobo" in wheel_name:
                for slot in wheel_values.get("slots", []):
                    wheel_slot = GoboWheelSlot("N/A")
                    if slot.get('type', "") == 'Open':
                        wheel_slot.name = 'Open'
                    elif slot.get('type', "") == 'Gobo':
                        wheel_slot.name = slot.get('name', "N/A")
                    gobowheel.append(wheel_slot)

    for channel in mode.get('channels', []):
        for col in ["Red", "Green", "Blue", "White", "Amber"]:
            if col in channel:
                setattr(colcap, col, True)

        if any(dim in channel for dim in ['Intensity', 'Dimmer']):
            cap.Intesity = True

        if "Pan" in channel:
            pan_info = json_dic.get('availableChannels', {}).get(channel, {})
            angleStart = 0
            if pan_info.get('capability'):
                if pan_info['capability'].get("angleStart"):
                    angleStart = int(
                        pan_info['capability']["angleStart"].split("deg")[0])
            angleEnd = 0
            if pan_info.get('capability'):
                if pan_info['capability'].get("angleEnd"):
                    angleEnd = int(
                        pan_info['capability']["angleEnd"].split("deg")[0])
            standardVal = int(pan_info.get('defaultValue', 0))
            if not cap.Pan or not any(cap.Pan):
                cap.Pan = (angleStart, angleEnd, standardVal)

        if "Tilt" in channel:
            tilt_info = json_dic.get('availableChannels', {}).get(channel, {})
            angleStart = 0
            if tilt_info.get('capability'):
                if tilt_info['capability'].get("angleStart"):
                    angleStart = int(
                        tilt_info['capability']["angleStart"].split("deg")[0])
            angleEnd = 0
            if tilt_info.get('capability'):
                if tilt_info['capability'].get("angleEnd"):
                    angleEnd = int(
                        tilt_info['capability']["angleEnd"].split("deg")[0])
            standardVal = int(tilt_info.get('defaultValue', 0))
            if not cap.Tilt or not any(cap.Tilt):
                cap.Tilt = (angleStart, angleEnd, standardVal)

    if colwheel:
        colcap.Wheel = colwheel
    if colcap:
        cap.Color = colcap
    if gobowheel:
        cap.Gobo = gobowheel

    typ_to_func_new[typ_mode_name] = cap
    return cap


def typ_to_modes_func_new(json_dic: 'dict[str, Any]') -> 'dict[str, LampCapabilities]':
    parsed: 'dict[str, LampCapabilities]'
    parsed = {}
    for mode in json_dic.get('modes', []):
        parsed[mode.get('name', 'N/A')] = mode_to_func_new(json_dic, mode)
    return parsed


def create_typ_to_func_new(json_names: 'list[str]') -> 'list[dict[str, LampCapabilities]]':
    parsed: 'list[dict[str, LampCapabilities]]'
    parsed = []
    for json_name in json_names:
        try:
            with open(json_name) as json_file:
                json_dic = json.load(json_file)
                parsed.append(typ_to_modes_func_new(json_dic))
        except FileNotFoundError:
            print("file {0:s} not found!".format(json_name))
    return parsed

#create_typ_to_func(["../dev/ofl-json/mac-250-krypton.json","../dev/ofl-json/generic/drgb-fader.json","../dev/ofl-json/generic/cmy-fader.json","../dev/ofl-json/generic/drgb-fader.json"])
#create_typ_to_func(["../dev/ofl-json/tao-led.json","../dev/ofl-json/generic/desk-channel.json","../dev/ofl-json/michi.json"])
#create_typ_to_addr(["../dev/ofl-json/tao-led.json","../dev/ofl-json/generic/desk-channel.json","../dev/ofl-json/michi.json"])

#with open("typ_to_func.json", 'w') as json_out:
    #json.dump(typ_to_func, json_out)

#with open("typ_to_addr.json", 'w') as json_out:
    #json.dump(typ_to_addr, json_out)
