from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

from GlobalVar import error_log_global, encoders

import serial

class FaderThread(QThread):
    keystroke = pyqtSignal(str, bool)
    fadermove = pyqtSignal(str, int)
    send_error = pyqtSignal(str)
    
    serial_send_buffer = ''
    
    def __init__(self):
        QThread.__init__(self)
        while 1:
            try:
                self.ser = serial.Serial("/dev/faderkeys", 115200)
                break
            except serial.serialutil.SerialException:
                error_log_global.append("SerialThread: Serial exception")
        self.ser.reset_input_buffer()
        self.serial_send_timer = QTimer(self)
        self.serial_send_timer.timeout.connect(self.send_serial)
        self.serial_send_timer.start(500)
    
    def run(self):
        while 1:
            try:
                serial_get = str(self.ser.readline())
                if "A" in serial_get:
                    spacer = serial_get.index(":")
                    self.fadermove.emit(serial_get[3:spacer], int(serial_get[spacer+1:-5]))
                else:
                    self.keystroke.emit(serial_get[2:-6], bool(int(serial_get[-6])))
            except:
                error_log_global.append("SerialThread: {0:s} failed".format(serial_get))
    
    @pyqtSlot(int, int)
    def set_fader(self, fader: int, value: int):
        self.serial_send_buffer += "A{0:d}:{1:d},".format(fader,value)
    
    def send_serial(self):
        if self.serial_send_buffer != '':
            self.serial_send_buffer+=";"
            self.ser.write(bytearray(self.serial_send_buffer.encode()))
            self.serial_send_buffer = ''

class EncoderThread(QThread):
    encodermove = pyqtSignal(str, int)
    send_error = pyqtSignal(str)

    def __init__(self):
        QThread.__init__(self)
        while 1:
            try:
                self.ser = serial.Serial("/dev/encoders", 115200)
                break
            except serial.serialutil.SerialException:
                error_log_global.append("SerialThread: Serial exception")
        self.ser.reset_input_buffer()
        self.last_vals = [0 for i in range(encoders)]

    def run(self):
        while 1:
            rea = self.ser.read(self.ser.inWaiting()).decode("utf-8").split('\r\n')
            if len(rea) >= 2:
                try:
                    enc = int(rea[-2].split(":")[0])
                    val = int(rea[-2].split(":")[1])
                    rotation = val - self.last_vals[enc]
                    self.last_vals[enc] = val
                    self.encodermove.emit(str(enc), rotation)
                except:
                    pass
