from typing import Union
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

import GlobalVar
import Lamp

from time import time

class CuelistThread(QThread):
    lampset = pyqtSignal(int, str, object)
    cuelist: GlobalVar.CueList
    cuelist = GlobalVar.CueList({}, [])
    options: GlobalVar.CuelistOptions
    options = {}
    cues: "list[GlobalVar.Cue]"
    cues = []

    fade = False
    cur_cue = 0
    next_cue = 0

    lamp_to_fade: "list[tuple[Lamp.Lamp, Lamp.LampState]]"
    lamp_to_fade = []

    def __init__(self):
        QThread.__init__(self)
        self.fade_timer=QTimer(self)
        self.fade_timer.timeout.connect(self.fade_cue)
        self.fade_timer.start(20)
        #self.global_timer.start(500)

    def set_cuelist(self, cuelist: str):
        try:
            self.cuelist = GlobalVar.cuelist_dict[cuelist]
            self.options = self.cuelist.options
            self.cues = self.cuelist.cues
        except KeyError:
            GlobalVar.error_log_global.append(
                "CuelistThread: cuelist {0:s} not defined".format(cuelist))

    def run(self):
        self.goto(0)
        loop = QEventLoop()
        loop.exec_()

    def fade_val(self, from_: Union[int, float], to_: Union[int, float], from_time: int, to_time: int, cur_time: int) -> float:
        a = (to_ - from_) / (to_time - from_time)
        c = (-a) * from_time + from_
        y = a * cur_time + c
        return y

    def fade_cue(self):
        if self.fade:
            for lamp in self.lamp_to_fade:
                lampstate = Lamp.LampState()
                try:
                    prev_lamp = self.cues[self.cur_cue][2][[
                        x[0] for x in self.cues[self.cur_cue][2]].index(lamp[0])]
                except ValueError:
                    prev_lamp = (Lamp.Lamp(), Lamp.LampState())
                for cap in list(set(list(prev_lamp[1].__dict__.keys()) + list(lamp[1].__dict__.keys()))):
                    if cap == "Color":
                        lampcol = Lamp.LampColor()
                        lampstate.Color = lampcol
                        cola = getattr(prev_lamp[1], "Color", Lamp.LampColor.from_rgb(0, 0, 0))
                        colb = getattr(lamp[1], "Color", Lamp.LampColor.from_rgb(0, 0, 0))
                        for col in list(set(list(cola.__dict__.keys()) + list(colb.__dict__.keys()))):
                            setattr(lampcol, f"{col}", self.fade_val(getattr(cola, col), getattr(colb, col), self.fade_time_begin, self.fade_time_end, time()))
                    else:
                        setattr(lampstate, cap, self.fade_val(getattr(prev_lamp[1], cap, 0), getattr(lamp[1], cap, 0), self.fade_time_begin, self.fade_time_end, time()))
                self.set_lamp(lamp[0], lampstate)
            if time() >= self.fade_time_end:
                for lamp in self.cues[self.next_cue][2]:
                    self.set_lamp(*lamp)
                self.fade=False
                self.cur_cue = self.next_cue

    def get_alter_lamps(self, current: "list[tuple[Lamp.Lamp, Lamp.LampState]]", _next: "list[tuple[Lamp.Lamp, Lamp.LampState]]") -> "list[tuple[Lamp.Lamp, Lamp.LampState]]":
        if _next == current:
            return current
        else:
            return_list: "list[tuple[Lamp.Lamp, Lamp.LampState]]"
            return_list = []
            for lamp in _next:
                if not lamp in current:
                    return_list.append(lamp)
            return return_list

    def set_lamp(self, lamp: Lamp.Lamp, lampstate: Lamp.LampState) -> None:
        for cap, val in lampstate.__dict__.items():
            if val:
                if type(val) == Lamp.LampColor:
                    for col in ["Red", "Green", "Blue", "White", "Amber", "UV"]:
                        if getattr(val, col)():
                            self.lampset.emit(lamp.getNum(), col, getattr(val, col)())
                elif cap in ["ColorWheelPosition", "GoboWheelPosition"]:
                    pass
                else:
                    self.lampset.emit(lamp.getNum(), cap, val)

    def goto(self, cue: int):
        self.paused = False
        if cue == -1:
            cue = self.cur_cue + 1
        if cue >= len(self.cues) and self.options.get("warp_at_end", False):
            cue = 0
        try:
            if self.cues[cue][1] == 0:
                for lamp in self.cues[cue][2]:
                    self.set_lamp(*lamp)
                self.cur_cue=cue
            else:
                cur_cue_list = self.cues[self.cur_cue][2]
                next_cue_list = self.cues[cue][2]
                self.lamp_to_fade = self.get_alter_lamps(cur_cue_list,next_cue_list)
                self.next_cue=cue
                self.fade_time_end = time() + self.cues[cue][1]
                self.fade_time_begin = time()
                self.fade = True
        except KeyError:
            GlobalVar.error_log_global.append(
                "CuelistThread: cue {0:d} not defined".format(cue))

    def go(self):
        if self.paused:
            delta = time() - self.paused_at
            self.fade_time_begin += delta
            self.fade_time_end += delta
            self.fade = True
            self.paused = False
        else:
            self.goto(-1)

    def stop(self):
        self.fade = False
        self.cur_cue = self.next_cue

    def pause(self):
        if not self.paused:
            self.paused = True
            self.fade = False
            self.paused_at = time()

    def release(self):
        for lamp_num in self.used_lamps:
            try:
                GlobalVar.nr_in_use[lamp_num] -= 1
                if GlobalVar.nr_in_use[lamp_num] == 1:
                    self.lampset.emit(lamp_num, "Dimmer", 0)
            except KeyError:
                GlobalVar.error_log_global.append(
                    "CuelistThread: failed to find lamp nr {0:d}".format(lamp_num))
