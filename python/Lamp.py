from typing import Any, Optional, Union

from PyQt5.QtGui import QColor

import colorsys


class GoboWheelSlot():
    Icon: Any
    name: str

    def __init__(self, name: str, Icon: Any = None) -> None:
        self.name = name
        self.Icon = Icon

    def hasIcon(self) -> bool:
        return bool(self.Icon)

    def __str__(self) -> str:
        return f"GoboWheelSlot(name={self.name})"

    def __repr__(self) -> str:
        return self.__str__()

    def __eq__(self, o: 'GoboWheelSlot') -> bool:
        return all([getattr(self, key) == getattr(o, key) for key in self.__dict__])

    def __ne__(self, o: 'GoboWheelSlot') -> bool:
        return not self.__eq__(o)

    def __delattr__(self, name: str) -> None:
        if (name == "Icon"):
            self.Icon = None
        elif (name == "name"):
            self.name = ""


class ColorWheelSlot():
    name: str
    color: QColor

    def __init__(self, name: str, color: QColor) -> None:
        self.name = name
        self.color = color

    def __str__(self) -> str:
        return f"ColorWheelSlot(name={self.name},color={self.color.getRgb()})"

    def __repr__(self) -> str:
        return self.__str__()

    def __eq__(self, o: 'ColorWheelSlot') -> bool:
        return all([getattr(self, key) == getattr(o, key) for key in self.__dict__])
    
    def __ne__(self, o: 'ColorWheelSlot') -> bool:
        return not self.__eq__(o)

    def __delattr__(self, name: str) -> None:
        if (name == "name"):
            self.name = ""
        elif (name == "color"):
            self.color = QColor()


class LampColorCapabilities():
    Red: bool
    Green: bool
    Blue: bool
    White: bool
    Amber: bool
    Uv: bool
    Wheel: Union[bool, 'list[ColorWheelSlot]']

    def __init__(self, Red: bool = False, Green: bool = False, Blue: bool = False, White: bool = False, Amber: bool = False, UV: bool = False, Wheel: Union[bool, 'list[ColorWheelSlot]'] = False) -> None:
        self.Red = Red
        self.Green = Green
        self.Blue = Blue
        self.White = White
        self.Amber = Amber
        self.Uv = UV
        self.Wheel = Wheel

    def __str__(self) -> str:
        return "LampColorCapabilities(" + ", ".join([f"{name}:{value}" for name, value in self.__dict__.items()]) + ")"

    def __repr__(self) -> str:
        return self.__str__()

    def __add__(self, o: 'LampColor') -> 'LampColor':
        return LampColor(*[getattr(self, key) or getattr(o, key) for key in self.__dict__])

    def __sub__(self, o: 'LampColor') -> 'LampColor':
        return LampColor(*[getattr(self, key) and not getattr(o, key) for key in self.__dict__])

    def __delattr__(self, name: str) -> None:
        if (name in self.__dict__):
            setattr(self, name, False)

    def __eq__(self, o: 'LampColorCapabilities') -> bool:
        return all([getattr(self, key) == getattr(o, key) for key in self.__dict__])

    def __ne__(self, o: 'LampColorCapabilities') -> bool:
        return not self.__eq__(o)

    def __bool__(self) -> bool:
        return any(self.__dict__.values())


class LampCapabilities():
    Intesity: bool
    Color: Union[bool, LampColorCapabilities]
    Gobo: Union[bool, 'list[GoboWheelSlot]']
    Pan: Union[bool, 'tuple[int, int, int]']
    Tilt: Union[bool, 'tuple[int, int, int]']

    def __init__(self, Intensity: bool = False, Color: Union[bool, LampColorCapabilities] = False, Gobo: Union[bool, 'list[GoboWheelSlot]'] = False, Pan: Union[bool, 'tuple[int, int, int]'] = False, Tilt: Union[bool, 'tuple[int, int, int]'] = False) -> None:
        self.Intesity = Intensity
        self.Color = Color
        self.Gobo = Gobo
        self.Pan = Pan
        self.Tilt = Tilt

    def __str__(self) -> str:
        return "LampCapabilities(" + ", ".join([f"{name}:{value}" for name, value in self.__dict__.items()]) + ")"

    def __repr__(self) -> str:
        return self.__str__()

    def __add__(self, o: 'LampCapabilities') -> 'LampCapabilities':
        return LampCapabilities(*[getattr(o, key) or getattr(self, key) for key in self.__dict__])

    def __sub__(self, o: 'LampCapabilities') -> 'LampCapabilities':
        return LampCapabilities(*[getattr(o, key) and not getattr(self, key) for key in self.__dict__])

    def __eq__(self, o: 'LampCapabilities') -> bool:
        return all([getattr(self, key) == getattr(o, key) for key in self.__dict__])
    
    def __ne__(self, o: 'LampCapabilities') -> bool:
        return not self.__eq__(o)

    def __delattr__(self, name: str) -> None:
        if (name in self.__dict__):
            setattr(self, name, False)


class LampColor():
    _calc_base: int
    _calc_base = 255
    _red: float
    _green: float
    _blue: float
    _white: float
    _amber: float
    _uv: float

    def __init__(self):
        pass

    def __str__(self) -> str:
        return "LampColor(" + ", ".join([f"{name}:{getattr(self, val, None)}" for name, val in [("Red", "_red"), ("Green", "_green"), ("Blue", "_blue"), ("White", "_white"), ("Amber", "_amber"), ("UV", "_uv")]]) + ")"

    def __repr__(self) -> str:
        return self.__str__()

    def __eq__(self, o: 'LampColor') -> bool:
        return all([getattr(self, key, None) == getattr(o, key, None) for key in ["_red", "_green", "_blue", "_white", "_amber", "_uv"]])

    def __ne__(self, o: 'LampColor') -> bool:
        return not self.__eq__(o)

    #region int
    @classmethod
    def from_rgb(cls, red: int, green: int, blue: int) -> 'LampColor':
        new = cls()
        new.setRed(red)
        new.setGreen(green)
        new.setBlue(blue)
        return new

    @classmethod
    def from_rgbw(cls, red: int, green: int, blue: int, white: int) -> 'LampColor':
        new = cls.from_rgb(red, green, blue)
        new.setWhite(white)
        return new

    @classmethod
    def from_white(cls, white: int) -> 'LampColor':
        new = cls()
        new.setWhite(white)
        return new

    @classmethod
    def from_amber(cls, amber: int) -> 'LampColor':
        new = cls()
        new.setAmber(amber)
        return new

    @classmethod
    def from_uv(cls, uv: int) -> 'LampColor':
        new = cls()
        new.setUV(uv)
        return new

    def setColor(self, name: str, val: int) -> None:
        if (0 <= val <= self._calc_base):
            self.setColorF(name, val / self._calc_base)
        else:
            raise ValueError(f"Value must be between 0 and {self._calc_base}")

    def setRed(self, red: int) -> None:
        self.setColor("red", red)

    def Red(self) -> int:
        try:
            return int(self._red * self._calc_base)
        except AttributeError:
            return 0

    def setGreen(self, green: int) -> None:
        self.setColor("green", green)

    def Green(self) -> int:
        try:
            return int(self._green * self._calc_base)
        except AttributeError:
            return 0

    def setBlue(self, blue: int) -> None:
        self.setColor("blue", blue)

    def Blue(self) -> int:
        try:
            return int(self._blue * self._calc_base)
        except AttributeError:
            return 0

    def setWhite(self, white: int) -> None:
        self.setColor("white", white)

    def White(self) -> int:
        try:
            return int(self._white * self._calc_base)
        except AttributeError:
            return 0

    def setAmber(self, amber: int) -> None:
        self.setColor("amber", amber)

    def Amber(self) -> int:
        try:
            return int(self._amber * self._calc_base)
        except AttributeError:
            return 0

    def setUV(self, uv: int) -> None:
        self.setColor("uv", uv)

    def UV(self) -> int:
        try:
            return int(self._uv * self._calc_base)
        except AttributeError:
            return 0

    def rgb(self) -> 'tuple[int, int, int]':
        return (self.Red(), self.Green(), self.Blue())
    #endregion

    #region float
    @classmethod
    def from_rgbf(cls, red: float, green: float, blue: float) -> 'LampColor':
        new = cls()
        new.setRedF(red)
        new.setGreenF(green)
        new.setBlueF(blue)
        return new

    @classmethod
    def from_rgbwf(cls, red: float, green: float, blue: float, white: float) -> 'LampColor':
        new = cls.from_rgbf(red, green, blue)
        new.setWhiteF(white)
        return new

    @classmethod
    def from_whitef(cls, white: float) -> 'LampColor':
        new = cls()
        new.setWhiteF(white)
        return new

    @classmethod
    def from_amberf(cls, amber: float) -> 'LampColor':
        new = cls()
        new.setAmberF(amber)
        return new

    @classmethod
    def from_uvf(cls, uv: float) -> 'LampColor':
        new = cls()
        new.setUVF(uv)
        return new

    def setColorF(self, name: str, val: float) -> None:
        if (0 <= val <= 1):
            setattr(self, f"_{name}", val)
        else:
            raise ValueError("Value must be between 0 and 1")

    def setRedF(self, red: float) -> None:
        self.setColorF("red", red)

    def RedF(self) -> float:
        try:
            return self._red
        except AttributeError:
            return 0.0

    def setGreenF(self, green: float) -> None:
        self.setColorF("green", green)

    def GreenF(self) -> float:
        try:
            return self._green
        except AttributeError:
            return 0.0

    def setBlueF(self, blue: float) -> None:
        self.setColorF("blue", blue)

    def Bluef(self) -> float:
        try:
            return self._blue
        except AttributeError:
            return 0.0

    def setWhiteF(self, white: float) -> None:
        self.setColorF("white", white)

    def WhiteF(self) -> float:
        try:
            return self._white
        except AttributeError:
            return 0.0

    def setAmberF(self, amber: float) -> None:
        self.setColorF("amber", amber)

    def AmberF(self) -> float:
        try:
            return self._amber
        except AttributeError:
            return 0.0

    def setUVF(self, uv: float) -> None:
        self.setColorF("uv", uv)

    def UVF(self) -> float:
        try:
            return self._uv
        except AttributeError:
            return 0.0

    def rgbf(self) -> 'tuple[float, float, float]':
        return (self.RedF(), self.GreenF(), self.Bluef())
    #endregion

    #region hsv
    @classmethod
    def from_hsv(cls, hue: float, saturation: float, value: float) -> 'LampColor':
        return cls.from_rgbf(*colorsys.hsv_to_rgb(hue, saturation, value))

    def hsv(self) -> 'tuple[float, float, float]':
        return colorsys.rgb_to_hsv(self.RedF(), self.GreenF(), self.Bluef())

    def Hue(self) -> float:
        return self.hsv()[0]

    def Saturation(self) -> float:
        return self.hsv()[1]

    def Value(self) -> float:
        return self.hsv()[2]
    #endregion

    def setResolution(self, res: int) -> None:
        self._calc_base = res


class LampState():
    Intensity: Union[int, float]
    Color: LampColor
    ColorWheelPosition: Union[int, float]
    GoboWheelPosition: Union[int, float]
    Pan: int
    Tilt: int

    def __init__(self, Intensity: Optional[Union[int, float]] = None, Color: Optional[LampColor] = None, ColorWheelPosition: Optional[Union[int, float]] = None, GoboWheelPosition: Optional[Union[int, float]] = None, Pan: Optional[int] = None, Tilt: Optional[int] = None) -> None:
        if Intensity:
            self.Intensity = Intensity
        if Color:
            self.Color = Color
        if ColorWheelPosition:
            self.ColorWheelPosition = ColorWheelPosition
        if GoboWheelPosition:
            self.GoboWheelPosition = GoboWheelPosition
        if Pan:
            self.Pan = Pan
        if Tilt:
            self.Tilt = Tilt

    def __str__(self) -> str:
        return "LampState(" + ", ".join([f"{name}:{value}" for name, value in self.__dict__.items()]) + ")"

    def __repr__(self) -> str:
        return self.__str__()

    def __add__(self, o: 'LampState') -> 'LampState':
        return LampState(*[getattr(self, key) or getattr(o, key) for key in self.__dict__])

    def __eq__(self, o: 'LampState') -> bool:
        return all([getattr(self, key) == getattr(o, key) for key in self.__dict__])

    def __ne__(self, o: 'LampState') -> bool:
        return not self.__eq__(o)


class LampAddress():
    Universe: int
    Address: int

    def __init__(self, Universe: int, Address: int) -> None:
        self.Universe = Universe
        self.Address = Address

    def __str__(self) -> str:
        return "LampAddress(" + ", ".join([f"{name}:{value}" for name, value in self.__dict__.items()]) + ")"

    def __repr__(self) -> str:
        return self.__str__()

    def __eq__(self, o: 'LampAddress') -> bool:
        return self.Address == o.Address and self.Universe == o.Universe

    def __ne__(self, o: 'LampAddress') -> bool:
        return not self.__eq__(o)


class Lamp():
    Capabilities: LampCapabilities
    State: LampState
    Address: LampAddress
    Name: str
    Typ: str
    _Num: int

    def __init__(self, Num: int, Capabilities: LampCapabilities = LampCapabilities(), Name: str = "", Typ: str = "", State: Optional[LampState] = None, Address: Optional[LampAddress] = None) -> None:
        self._Num = Num
        self.Capabilities = Capabilities
        self.Name = Name
        self.Typ = Typ
        if State:
            self.State = State
        if Address:
            self.Address = Address

    def __str__(self) -> str:
        return "LampCapabilities(" + ", ".join([f"{name}:{value}" for name, value in self.__dict__.items()]) + ")"

    def __repr__(self) -> str:
        return self.__str__()

    def __eq__(self, o: 'Lamp') -> bool:
        return self.getNum() == o.getNum() and self.getTyp() == o.getTyp()

    def __ne__(self, o: 'Lamp') -> bool:
        return not self.__eq__(o)

    def hasIntensity(self) -> bool:
        return self.Capabilities.Intesity

    def hasColor(self) -> bool:
        return bool(self.Capabilities.Color)

    def hasColorWheel(self) -> bool:
        return type(self.Capabilities.Color) == list

    def hasGobo(self) -> bool:
        return bool(self.Capabilities.Gobo)

    def hasPan(self) -> bool:
        return bool(self.Capabilities.Pan)

    def hasTilt(self) -> bool:
        return bool(self.Capabilities.Tilt)

    def Intensity(self) -> Union[int, float]:
        return self.State.Intensity

    def Color(self) -> LampColor:
        return self.State.Color

    def ColorWheel(self) -> Union[float, ColorWheelSlot, None]:
        pos = self.State.ColorWheelPosition
        if type(pos) == int:
            if self.Capabilities.Color and self.Capabilities.Color.Wheel:
                return self.Capabilities.Color.Wheel[pos]
            else:
                return None
        else:
            return pos

    def Gobo(self) -> Union[float, GoboWheelSlot, None]:
        pos = self.State.GoboWheelPosition
        if type(pos) == int:
            if self.Capabilities.Gobo:
                return self.Capabilities.Gobo[pos]
            else:
                return None
        else:
            return pos

    def Pan(self) -> int:
        return self.State.Pan

    def Tilt(self) -> int:
        return self.State.Tilt

    def getNum(self) -> int:
        return self._Num

    def getName(self) -> str:
        return self.Name

    def setName(self, name: str) -> None:
        self.Name = name

    def getTyp(self) -> str:
        return self.Typ

    def setTyp(self, typ: str) -> None:
        self.Typ = typ

    def getAddresss(self) -> LampAddress:
        return self.Address

    def setAddress(self, address: LampAddress) -> None:
        self.Address = address

    def getState(self) -> LampState:
        return self.State

    def setState(self, state: LampState) -> None:
        self.State = state

    def getCapabilities(self) -> LampCapabilities:
        return self.Capabilities

    def setCabailities(self, cababilities: LampCapabilities) -> None:
        self.Capabilities = cababilities
