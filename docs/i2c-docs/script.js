function set_none_active() {
    var list = document.getElementsByClassName("side-panel")[0].getElementsByTagName("ul")[0].getElementsByTagName("li")
    for (let i = 0; i < list.length; i++) {
        const element = list[i];
        element.classList.remove("active");
    }
}

function set_active(id) {
    set_none_active();
    document.getElementById(id).classList.add("active");
}

var curr_file = document.location.pathname.split("/").slice(-1)[0].split(".")[0];
set_active(curr_file);
document.title = document.getElementById(curr_file).innerText;
