from machine import Pin, I2C
import time
import sys
import json

i2c = I2C(0, scl=Pin(1), sda=Pin(0), freq=2000000)

input("press enter")
address = i2c.scan()[0]
print(address)

def handle_command(data):
    i2c.writeto(address, bytearray([data]))

def command_wrapper(string):
    handle_command(int(string, 2))

def split_length(length):
    string = "{0:016b}".format(length)
    return (int(string[:8], 2), int(string[8:], 2))

#Ping
if True:
    print("trying ping")
    command_wrapper("00000000")
    reply = i2c.readfrom(address, 1)
    if reply and reply[0] == 255:
        print("Ping successfull")
#Change Address
if False:
    print("trying to change address")
    command_wrapper("00000001")
    command_wrapper("01011111")
    time.sleep(5)
    address = i2c.scan()[0]
    print("new address", address)
#Get Version
if False:
    print("trying Version Number")
    command_wrapper("00000010")
    reply = i2c.readfrom(address, 1)
    if reply and len(reply) > 0:
        print("Version is", reply[0])
#Update Config
if False:
    print("trying update config")
    command_wrapper("00110000")
    config = {
        "address": 96,
        "inputs": {
            "buttons": {
                "rows": {
                    "number": 5,
                },
                "columns": {
                    "number": 4,
                }
            },
            "encoders": {
                "number": 3,
            },
            "groups": {
                "number": 5,
            }
        }
    }
    new_config = json.dumps(config)
    handle_command(0)
    handle_command(len(new_config))
    for char in new_config:
        handle_command(ord(char))
        time.sleep(0.005)
    time.sleep(5)
    address = i2c.scan()[0]
    print("new address", address)
#Update Main
if False:
    print("trying update main")
    command_wrapper("00110001")
    with open("slave.py") as file_:
        new_config = file_.read()
    # new_config = 'input("press return")\nprint("hurra!")'
    len_split = split_length(len(new_config))
    handle_command(len_split[0])
    handle_command(len_split[1])
    for char in new_config:
        handle_command(ord(char))
        time.sleep(0.005)
    time.sleep(5)
#Update Boot
if False:
    print("trying update boot")
    command_wrapper("00110010")
    with open("boot.py") as file_:
        new_config = file_.read()
    # new_config = 'input("press return")\nprint("hurra!")'
    len_split = split_length(len(new_config))
    handle_command(len_split[0])
    handle_command(len_split[1])
    for char in new_config:
        handle_command(ord(char))
        time.sleep(0.005)
    time.sleep(5)
#Soft Reset
if False:
    print("trying soft reset")
    command_wrapper("00111110")
    time.sleep(2)
#Hard Reset
if False:
    print("trying hard reset")
    command_wrapper("00111111")
    time.sleep(2)
#Bootloader Reset
if False:
    print("trying bootloader reset")
    command_wrapper("00111100")
    sys.exit(0)

#Get Capabilities
if True:
    print("trying cabailities")
    command_wrapper("01000000")
    reply = i2c.readfrom(address, 1)
    if reply and len(reply) > 0:
        form = "{0:08b}".format(reply[0])
        print("capbailities", form)
        if form[0] == "1":
            print("Has Groups")
        if form[4] == "1":
            print("Has 2-axis")
        if form[5] == "1":
            print("Has Encoders")
        if form[6] == "1":
            print("Has Analog")
        if form[7] == "1":
            print("Has Buttons")
            print("trying button matrix")
            command_wrapper("01000001")
            reply1 = i2c.readfrom(address, 1)
            if reply1 and len(reply1) > 0:
                form1 = "{0:08b}".format(reply1[0])
                print("Has {0} Rows and {1} Columns".format(
                    int(form1[:4], 2), int(form1[4:], 2)))
