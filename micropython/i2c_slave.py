from i2c_responder import I2CResponder
import machine
import json

try:
    with open("config.json") as config_file:
        config = json.load(config_file)
except:
    config = {}
config["version"] = 1
print("config", config)

RESPONDER_I2C_DEVICE_ID = 0
# RESPONDER_ADDRESS = 0x08
RESPONDER_ADDRESS = config.get("address", 0x08)
GPIO_RESPONDER_SDA = 0
GPIO_RESPONDER_SCL = 1
i2c_responder = I2CResponder(RESPONDER_I2C_DEVICE_ID, sda_gpio=GPIO_RESPONDER_SDA, scl_gpio=GPIO_RESPONDER_SCL, responder_address=RESPONDER_ADDRESS)

def save_config():
    with open("config.json", "w") as config_file:
        json.dump(config, config_file)

def get_i2c_data(length):
    data = []
    while len(data) < length:
        temp = i2c_responder.get_write_data(1)
        if temp and len(temp) > 0:
            data.append(temp[0])
    return data

def update_file(name):
    file_length = get_i2c_data(2)
    if file_length and len(file_length) > 1:
        length = int("{0:08b}{1:08b}".format(*file_length), 2)
        new_file = get_i2c_data(length)
        config_file = open(name, "w")
        for char in new_file:
            config_file.write(chr(char))
        config_file.close()
        machine.soft_reset()

def handle_control(data):
    #Ping
    if data == "000000":
        i2c_responder.put_read_data(0xff)
    #Change Address
    elif data == "000001":
        newaddress = i2c_responder.get_write_data(1)
        if newaddress and len(newaddress) > 0:
            config["address"]=newaddress[0]
            save_config()
            machine.soft_reset()
    #Get Version
    elif data == "000010":
        version = config.get("version", 0)
        i2c_responder.put_read_data(version)
    #Update Config File
    elif data == "110000":
        update_file("config.json")
    #Update Main File
    elif data == "110001":
        update_file("main.py")
    #Update Boot File
    elif data == "110010":
        update_file("boot.py")
    #Reset to Bootloader
    elif data == "111100":
        machine.bootloader()
    #Soft Reset
    elif data == "111110":
        machine.soft_reset()
    #Hard Reset
    elif data == "111111":
        machine.reset()

#TODO:
def handle_input(data):
    #Get Capabilities
    if data == "000000":
        response = [0]*8
        if config.get("groups", False) and any(x.get("inputs", False) for x in config["groups"]):
            response[0] = 1
        if config.get("inputs", False):
            for i, input_t in enumerate(["2-axis", "encoders", "analog"]):
                if config["inputs"].get(input_t, False) and config["inputs"][input_t].get("number", 0) > 0:
                    response[i+4] = 1
            if config["inputs"].get("buttons", False) and config["inputs"]["buttons"].get("rows", False) and config["inputs"]["buttons"].get("columns", False):
                response[-1] = 1
        i2c_responder.put_read_data(int("".join(str(x) for x in response), 2))
    #Get Button Matrix
    if data == "000001":
        response = "0"
        if config.get("inputs", False) and config["inputs"].get("buttons", False):
            if config["inputs"]["buttons"].get("rows", False) and len(config["inputs"]["buttons"]["rows"]) > 0:
                response += "{0:03b}".format(len(config["inputs"]["buttons"]["rows"]))
            else:
                response += "000"
            if config["inputs"]["buttons"].get("columns", False) and len(config["inputs"]["buttons"]["columns"]) > 0:
                response += "{0:04b}".format(len(config["inputs"]["buttons"]["columns"]))
            else:
                response += "0000"
        i2c_responder.put_read_data(int(response, 2))

#TODO:
def handle_output(data):
    pass

def handle_command(data):
    bin_data = "{0:08b}".format(data)
    if bin_data[:2] == "00":
        handle_control(bin_data[2:])
    elif bin_data[:2] == "01":
        handle_input(bin_data[2:])
    elif bin_data[:2] == "10":
        handle_output(bin_data[2:])

while True:
    if i2c_responder.write_data_is_available():
        temp = i2c_responder.get_write_data(1)
        if temp and len(temp) > 0:
            handle_command(temp[0])
