from machine import Pin, I2C

i2c = I2C(0, scl=Pin(1), sda=Pin(0), freq=2000000)

while True:
    print("scan", i2c.scan())
    for i in range(256):
        i2c.writeto(8, bytearray([i]))
        print((i, i2c.readfrom(8, 1)))
