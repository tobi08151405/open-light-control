from machine import Pin

direction = [Pin(14, Pin.IN)]
pulse = [Pin(15, Pin.IN)]

laststate = [False]
curnum = [0]

while True:
    for num in range(len(direction)):
        curvalue = pulse[num].value()
        if (not laststate[num]) and curvalue:
            if direction[num].value():
                curnum[num] -= 1
            else:
                curnum[num] += 1
            print(str(num) + ":" + str(curnum[num]))
        laststate[num] = curvalue
